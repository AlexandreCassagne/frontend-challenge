# MY01 Code Challenge

## Welcome to the MY01 Code Challenge!

Ce fichier est aussi disponible en Français. This README is also available in French.

We use code challenges to verify, objectively, that certain requirements are met by our candidates. We believe this is a fairer
way of assessing our candidates skills than the interview process, which, while they allow us to assess your ability to answer
questions under more pressure, do not match the "reality" of a developer's day to day.

Please make sure you read the guidance. You should dedicate less than 3-4 hours to this project.

### Logistics

Use Git for version control (you can use GitHub or Gitlab; if you prefer, feel free to use any open source publicly accessible
system).

Please use descriptive commits! And try to use Git branches if you know how to, to group similar commits (for instance, when you
try 15 commits to get it right, then you want to "squash" all the commits into one).

A simple example (but feel free to use your own)

```
master
|__ feature 1
|__ feature 2
|__ ...
```

## Project Description

You are working at a law firm, and report directly to a Partner tasked with the new web portal. She asks you to provide a web
page for tracking a contract's status. The contract follows the following statuses:

1. **Specifications Provided** (Customer has written a request for a contract, either by typing free-text or by choosing from a
   drop-down from a set of standard contracts *OR* Customer has rejected a proposal and requested changes);
2. **Proposal Ready** (A proposed contract was submitted by one of the Associates at the law firm, and it is viewable by the
   Customer). The contract can be seen (title, last modified date, and contract text). The Customer can "Approve" or "Reject"
   (reject must include a message so that the lawyer can make the desired changes);
3. **Proposal Accepted** (The customer is charged for all the billable hours; an email with a PDF attachment of the finalised
   contract is sent to the customer with nice message from the Partner. Then, the contract is transferred to the Marketing team,
   where they study how they can sell the custom contract to a wider audience…).

Today, the Partner asks you to propose a UI for Screen 2, *"Proposal Ready"*.
(**IMPORTANT:** Only develop Screen 2!)

She walks away without giving you many details about what the contract looks like; but she does give you the
attached `contract.json`. It is described in the section named "Date you're working with".

The partner is very fussy about text justification for contract clauses--you point out that text justification is difficult on
browsers because of hyphenation--she recommends you look at [hyphenator.js][1]. It creates a `.hyphenate` CSS class that can be
used on browsers that don't support hyphenation.

#### General Layout

Your page should contain a "navbar" and "footer". Keep them super simple! I don't care if they are fixed, coloured, what size
they are, etc. as long as it is reasonably legible.

The footer contains "Copyright 2021 Mike & Anna's Law Inc."; the header simply contains the logo in the top left, which is the
text "Mike and Anna's Law" in a sans-serif font.

The company's primary colour is `#005A92` (the logo is in this colour).

#### Responsivity

The screen should be responsive.

#### Test

There should be at least one UI test or Unit test.

#### Guidance

Don't overthink. My goal is to assess your ability to work independently. Nothing needs to be perfect. Focus your attention on
overall flow (don't try to customise buttons to perfection; I don't expect the most incredible colour palette, but text should
be contrasted well to be legible). In short, focus on the most important aspects for the customer.

I ask about `hyphenator.js` simply to test your ability to integrate CSS/JS code from a third party manually, etc… I don't
expect you to customise it or spend more than 5 minutes on this.

### Data you're working with

This is purely a front-end exercise; there is no server-side code to implement. A separate JSON file is attached. You will see
that there are substitutable variables in the
`clauses` (clauses are simply `<p>aragraphs</p>` that are justified and `.hyphenated`).

##### Clause-Substitution Variable Syntax

The syntax for the substitutable variables is…

If, the variables key of the attached JSON file shows:

```json
{
  "substitutionVariables": {
    "var1": "Michael",
    "var2": "Smith"
  },
  "sections": [
    {
      "id": "body-1",
      "clauses": [
        "Welcome, <var1> <var2>!"
      ]
    }
  ]
}
```

Then

`"Welcome, <var1> <var2>!"` becomes `"Welcome, Michael Smith!"`

##### Metadata

Clauses are given an id. The id should render in the `<p>`'s id property, _e.g_:

```html

<div <!-- ... -->>
<p id="body-1" <!-- other props... -->>Welcome, Michael Smith!</p>
</div>
```

[1]: https://mnater.github.io/Hyphenator/